# Mermaid Note

## 饼图 PieChart

```mermaid
pie
title 数值比例图
"A" : 25
"B" : 60
"C" : 15
```
## 流程图

```mermaid
graph LR
A-->B
B-->C
C-->D
D-->E
```

## 时序图
```mermaid
sequenceDiagram
participant User
participant System
User->>System: 发送请求
System->>User: 返回相应
```

## 状态图
```mermaid
stateDiagram
[*]-->暂停
暂停-->播放
暂停-->停止
播放-->暂停
播放-->停止
停止-->[*]
```
