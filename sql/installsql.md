# 数据库安装流程

## 安装环境说明
- 系统 `Centos Stream 9`
- 架构 `X86-64`
- 版本 `Mysql Community Server 9.0.1`
- 安装方法 `离线安装`

## 获取Mysql离线安装包
- 通过`https://dev.mysql.com/downloads/mysql`挑选安装版本
- Select Version: `9.0.1 Innovation`
- Select Operating System: `Red Hat Enterprise Linux / Oracle Linux`
- Select OS Version: `Red Hat Enterprise Linux9 / Oracle Linux9(x86,64-bit)`
- 分别选取下列rpm包进行下载
	- mysql-community-common-9.0.1-1.el9.x86_64.rpm
	- mysql-community-client-plugins-9.0.1-1.el9.x86_64.rpm
	- mysql-community-libs-9.0.1-1.el9.x86_64.rpm
	- mysql-community-client-9.0.1-1.el9.x86_64.rpm
	- mysql-community-icu-data-files-9.0.1-1.el9.x86_64.rpm
	- mysql-community-server-9.0.1-1.el9.x86_64.rpm

## Mysql安装步骤
- 使用`rpm -ivh`对上方六个rpm包根据报错提示调整顺序进行安装
- 安装过程中可能提示有针对`mariadb-connector-c-config-3.2.6-1.el9.noarch`包的冲突, 使用`rpm -e mariadb-connector-c-config-3.2.6-1.el9.noarch`进行卸载操作
- 查看`/var/log/mysqld.log`中复杂密码部分, 可以使用`cat /var/log/mysqld.log | grep password`筛出
- **Mysql在Centos系统中服务名为mysqld.service**

## Mysql初始配置流程
- 使用`systemctl start mysqld`进行Mysqld服务启动
- 使用`mysql -u root -p`进入mysql服务

### Mysql进行修改密码
- `use mysql;` 进入mysql数据库
- `flush privileges;` 刷新权限表, 用来确认user表的修改
- `set password for root@localhost = password('NewPassword');` 重新设定密码
- **忘却Mysql密码时可以在`/etc/my.cnf`文件中[mysqld]下添加`skip-grant-tables`并重启服务来跳过输入密码**

## Mysql开启允许远程
- Mysql服务器默认不允许除本地服务器外的设备登陆, 因此需要创建可远程用户
- `create user '<RemoteUser>'@'localhost' identified by 'Password';` 创建新用户
- `grant all privileges on *.* to '<RemoteUser>'@'localhost' with grant option;` 赋予该用户所有权限
- `flush privileges;` 刷新权限表使权限生效

## Mysql远程连接
- 在本地计算机中安装MysqlClient命令行工具
- 使用`mysql -h <RemoteHost> -u <User> -P <RemotePort> -p`来远程连接
